<?php get_header(); ?>

<main class="site-main site-main--article clearfix">
	<div class="article-img">
		<div class="container">
			<div class="row">
				<div class="article-img__wrapper">
					<picture>
						<?php
						if ( has_post_thumbnail() ) {
							echo get_the_post_thumbnail( $post->ID, 'post-header', array() );
						} else {
							?>
							<source srcset="<?php echo get_template_directory_uri(); ?>/img/1170x500.png 1x, <?php echo get_template_directory_uri(); ?>/img/2340x1000.png 2x"
							        media="(min-width: 1200px)">
							<source srcset="<?php echo get_template_directory_uri(); ?>/img/768x500.png 1x, <?php echo get_template_directory_uri(); ?>/img/1536x1000.png 2x"
							        media="(min-width: 768px)">
							<source srcset="<?php echo get_template_directory_uri(); ?>/img/480x500.png 1x, <?php echo get_template_directory_uri(); ?>/img/960x1000.png 2x"
							        media="(min-width: 480px)">
							<source srcset="<?php echo get_template_directory_uri(); ?>/img/320x500.png 1x, <?php echo get_template_directory_uri(); ?>/img/640x1000.png 2x"
							        media="(max-width: 479px)"><img src="<?php echo get_template_directory_uri(); ?>/img/index/s1x2.jpg" alt="">
							<?php
						}
						?>

					</picture>
					<div class="article-img__title">
						<h1><?php echo apply_filters( 'the_title', $post->post_title, $post->ID ); ?></h1>
						<p><?php matchmaker_posted_on(); ?></p>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="article-content">
		<div class="article-content__main">
			<?php echo apply_filters( 'the_content', $post->post_content ); ?>
		</div>

		<div class="article-content__sidebar">
			<div class="sidebar-subscribe">
				<?php echo do_shortcode( '[contact-form-7 id="103" title="Sing up"]' ); ?>
			</div>
			<div class="sidebar-share">
				<p>Share</p>
				<div class="sidebar-share__wrapper">
					<?php
					if ( function_exists( 'ot_get_option' ) ) { 
						$facebook_url = ot_get_option( 'social_facebook_link', '#' );
						$twitter_url  = ot_get_option( 'social_twitter_link', '#' );
						$g_plus_url   = ot_get_option( 'social_google_link', '#' );
						$linkedin_url = ot_get_option( 'social_linkedin_link', '#' ); 
					}
					?>
					<a href="<?php echo $facebook_url; ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/fb.svg" alt="" width="20"
						     height="20">
					</a>
					<a href="<?php echo $twitter_url; ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tw.svg" alt="" width="20"
						     height="18">
					</a>
					<a href="<?php echo $g_plus_url; ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/gp.svg" alt="" width="28"
						     height="18">
					</a>
					<a href="<?php echo $linkedin_url; ?>" target="_blank">
						<img src="<?php echo get_template_directory_uri(); ?>/img/li.svg" alt="" width="20"
						     height="18">
					</a>
				</div>
			</div>
			<?php
			if ( ! function_exists( 'dynamic_sidebar' ) || ! dynamic_sidebar( 'sidebar-1' ) ):
			endif;
			?>

		</div>
	</div>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>


<?php get_footer(); ?>
