<?php get_header(); ?>

<main class="site-main clearfix">
	<div class="container">
		<div class="learn-more-text">
			<div class="container">
				<div class="learn-more-text__wrapper">

					<?php
					while ( have_posts() ) : the_post();
						echo the_title( '<h1>', '</h1>', true );

						the_content();

					endwhile;
					?>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>

<?php get_footer(); ?>
