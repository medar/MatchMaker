<?php
/**
 * Template Name: Selectee
 *
 * Description: Template for get started page
 */
get_header(); ?>

<main class="site-main site-main--get-started clearfix">
	<div class="get-started-form">
		<?php echo get_field('selectee_iframe'); ?>
	</div>
</main>


<?php get_footer(); ?>
