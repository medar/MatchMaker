<?php
/**
 * Template Name: Confidentiality
 *
 * Description: Template for confidentiality page
 */
get_header(); ?>
<main class="site-main site-main--confidentiality clearfix">
	<div class="confidentiality-text">
		<div class="container">
			<div class="confidentiality-text__wrapper">

<?php
while (have_posts()) : the_post();

the_content();


endwhile; // End of the loop.
?>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>


<?php get_footer(); ?>

