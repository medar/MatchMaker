<?php
/**
 * Template Name: Get Started
 *
 * Description: Template for get started page
 */
get_header(); ?>

<main class="site-main site-main--love-lottery clearfix">
	<?php
	while ( have_posts() ) : the_post();

		?>
		<div class="love-lottery-content">
			<div class="lottery">
				<div class="container">
					<?php echo the_title( '<h1>', '</h1>', true ); ?>
					<?php the_content(); ?>

				</div>
			</div> 
			<div class="lottery lottery--second">
				<div class="container">
					<div class="lottery__wrapper">
						<div class="lottery__item">
							<h3>Be a Selector</h3>
							<p><?php echo get_field('selector'); ?></p><a class="btn btn--turquoise" href="/selector">Get Started </a>
						</div>
						<div class="lottery__item">
							<h3>Be a Selectee</h3>
							<p><?php echo get_field('selectee'); ?></p><a class="btn btn--turquoise" href="/selectee">Get Started </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="love-lottery-cite cite">
			<div class="container">
				<div class="cite__wrapper">
					<h3>“Be in it to Win it”</h3>
				</div>
			</div>
		</div>
		<?php

	endwhile;
	?>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>


<?php get_footer(); ?>
