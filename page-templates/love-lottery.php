<?php
/**
 * Template Name: Love Lottery
 *
 * Description: Template for love-lottery page
 */
get_header(); ?>

<main class="site-main site-main--love-lottery clearfix">
	<?php
	while ( have_posts() ) : the_post();

		?>
		<div class="love-lottery-content">
			<div class="lottery">
				<div class="container">
					<?php echo the_title( '<h1># ', '</h1>', true ); ?>
					<?php the_content(); ?>

				</div>
			</div>
			<div class="love-lottery-content__img">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/lottery/Faces_desktop.png 1x, <?php echo get_template_directory_uri(); ?>/img/lottery/Faces_desktop@2x.png 2x"
					        media="(min-width: 992px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/lottery/Faces_tablet.png 1x, <?php echo get_template_directory_uri(); ?>/img/lottery/Faces_tablet@2x.png 2x"
					        media="(min-width: 480px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/lottery/Faces_mobile.png 1x, <?php echo get_template_directory_uri(); ?>/img/lottery/Faces_mobile@2x.png 2x"
					        media="(max-width: 479px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/lottery/Faces_desktop@2x.png" alt="">
				</picture>
			</div>
			<div class="lottery lottery--second">
				<div class="container">
					<div class="lottery__wrapper">
						<div class="lottery__item">
							<h3>Be a Selector</h3>
							<p><?php echo get_field('selector'); ?></p><a class="btn btn--turquoise" href="/selector">Get Started </a>
						</div>
						<div class="lottery__item">
							<h3>Be a Selectee</h3>
							<p><?php echo get_field('selectee'); ?></p><a class="btn btn--turquoise" href="/selectee">Get Started </a>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="love-lottery-cite cite">
			<div class="container">
				<div class="cite__wrapper">
					<h3>“Be in it to Win it”</h3>
				</div>
			</div>
		</div>
		<?php

	endwhile;
	?>

	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>


<?php get_footer(); ?>
