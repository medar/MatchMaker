<?php
/**
 * Template Name: Matcher
 *
 * Description: Template for matcher page
 */
get_header(); ?>
	<main class="site-main site-main--matcher clearfix">

		<?php
		while ( have_posts() ) : the_post();

			?>
			<div class="matcher-text">
				<div class="container">
					<?php echo the_title( '<h1>', '</h1>', true ); ?>
					<div class="matcher-text__wrapper">
						<div class="matcher-text__text">
							<?php the_content(); ?>

						</div>
						<div class="matcher-text__photo">
							<?php echo get_the_post_thumbnail( $post->ID, 'matcher' ) ?>
						</div>
					</div>

				</div>
			</div>
			<div class="qa qa--matcher">
				<div class="container">
					<div class="qa__wrapper">
						<?php
						for ( $item = 1; $item <= 5; $item ++ ) {
							$q = get_field( "question-{$item}" );
							$a = get_field( "answer-{$item}" );


							if ( ! empty( $q ) or ! empty( $a ) ):
								echo '<div class="qa__item">';
								echo "<h2>{$q}</h2>";
								echo "<p>{$a}</p>";
								echo '</div>';
							endif;


						}
						?>
					</div>
				</div>
				<a class="btn btn--white" href="/get-started">Get Started </a>
			</div>


			<?php

		endwhile;
		?>

		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php matchmaker_subscribe(); ?>
	</main>


<?php get_footer(); ?>