<?php
/**
 * Template Name: Learn more
 *
 * Description: Template for learn more page
 */
get_header(); ?>
	<main class="site-main site-main--learn-more clearfix">

		<?php
		while ( have_posts() ) : the_post();
			?>
			<div class="learn-more-text">
				<div class="container">
					<div class="learn-more-text__wrapper">
						<?php echo the_title( '<h1>', '</h1>', true ); ?>
						<?php the_content(); ?>
					</div>

				</div>
			</div>
			<div class="qa qa--qa--learn-more">
				<div class="container">
					<div class="qa__wrapper">
						<?php
						for ( $item = 1; $item <= 5; $item ++ ) {
							$q = get_field( "question-{$item}" );
							$a = get_field( "answer-{$item}" );


							if ( ! empty( $q ) or ! empty( $a ) ):
								echo '<div class="qa__item">';
								echo "<h2>{$q}</h2>";
								echo "<p>{$a}</p>";
								echo '</div>';
							endif;


						}
						?>
					</div>
				</div>
				<a class="btn btn--white" href="/get-started">Get Started </a>
			</div>


			<?php

		endwhile;
		?>

		<?php get_template_part( 'template-parts/recent-posts' ); ?>
		<?php matchmaker_subscribe(); ?>
	</main>


<?php get_footer(); ?>