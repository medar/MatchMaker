<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<script type="text/javascript">
        //<![CDATA[
        var ajaxurl = "<?php echo admin_url( 'admin-ajax.php' ); ?>";
        //]]>
	</script>
	<?php wp_head(); ?>
	<link href="<?php echo get_template_directory_uri(); ?>/img/favicon-32x32.png" rel="icon" type="image/x-icon">
</head>
<body <?php body_class(); ?>>
<header class="site-header <?php if (is_front_page() or is_home()): echo 'site-header--index'; endif; ?>">
	<div class="container">
		<div class="row">
			<div class="site-header__wrapper">
				<div class="menu"> 

					<?php  
						$logo_opt = '<li><a href="' . ot_get_option( 'love_lottery_link', '#' ) .'">' . ot_get_option( 'love_lottery_title', '# Love Lottery' ) .'</a></li>'; 
					wp_nav_menu(
						array(
							'theme_location'  => 'menu-1',
							'menu_id'         => 'primary-menu',
							'container'       => 'div',
							'container_class' => 'menu-nav',
							'items_wrap' => '<ul>'.$logo_opt.'<li><a href="/"><img src="' . get_template_directory_uri() .'/img/logo--header.svg" width="189" height="42"></a></li>%3$s</ul>'
						)
					);
					
					?>
					<div class="menu-button is-closed">
						<div class="menu-button__wrapper"><span></span><span></span><span></span></div>
					</div>
				</div>

			</div>
		</div>
	</div>
</header>