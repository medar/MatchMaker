jQuery(document).ready(function ($) {
// events on load
    window.onload = function () {
        openMenu();
        allSites();
        indexSlider();
        videoDetect();
    }


// events on scroll
    $(window).scroll(function () {
        fixedHeader();
    });


// events on resize
    window.onresize = function () {
        fixedHeader();
        videoDetect();
        closeMenu();
    }

// function declarations
    function fixedHeader() {
        var scrollPosition = $(window).scrollTop();
        var fixedHeaderMenu = $('.site-header');
        var fixedStartPosition = 1;
        if ((scrollPosition >= fixedStartPosition) && ($(window).width() > '1200')) {
            fixedHeaderMenu.addClass("is-fixed");
        } else {
            fixedHeaderMenu.removeClass("is-fixed");
        }
    }

    function openMenu() {
        $('.menu-button').click(function () {
            var menu = $('.menu-nav');
            var btn = $('.menu-button');
            var main = $('.site-main');
            if (btn.hasClass('is-closed')) {
                btn.removeClass('is-closed');
                menu.addClass('is-open');
                main.addClass('menu-is-open');
            }
            else {
                btn.addClass('is-closed');
                menu.removeClass('is-open');
                main.removeClass('menu-is-open');
            }
        });
    }

    function closeMenu() {
        var menu = $('.menu-nav');
        var btn = $('.menu-button');
        var main = $('.site-main');
        if ($(window).width() > '991') {
            btn.addClass('is-closed');
            menu.removeClass('is-open');
            main.removeClass('menu-is-open');
        }
    }

    function allSites() {
        $('body').waypoint({
            handler: function (direction) {
                if (direction === 'down') {
                    $('#allSites').addClass('active');
                }
                if (direction === 'up') {
                    $('#allSites').removeClass('active');
                }
            },
            offset: '-100%'
        });
    }

    function indexSlider() {
        console.log("slider");
        var owl = $(".reviews-item__slider");
        owl.owlCarousel({
            slideSpeed: 300,
            paginationSpeed: 400,
            singleItem: true,
            mouseDrag: true,
            touchDrag: true,
            autoPlay: 5000,
            pagination: false,
            autoHeight: true,
        });
        $(".reviews-item__slider--right").click(function () {
            owl.trigger('owl.next');
        })
        $(".reviews-item__slider--left").click(function () {
            owl.trigger('owl.prev');
        })
    }

    function videoDetect() {
        if ($('#video').length > 0) {
            if (($('#video').length > 0) && ($(window).width() > '1200')) {
                $('.index-first-screen__img').addClass('disabled');
                $('.index-first-screen__video').addClass('active');
                video.loop = true;
                video.play();
            }
            else {
                $('.index-first-screen__img').removeClass('disabled');
                $('.index-first-screen__video').removeClass('active');
                video.loop = true;
                video.pause();
            }
        }
    }

    var $infinitescroll_btn = $('#infinitescroll-on_click');

    hide_infinite({action: 'infinite_scroll', offset: get_offset(), sort_by: $infinitescroll_btn.attr("data-sort")});

    function get_offset() {
        return $('.latest-blog-item').length;
    }
    $infinitescroll_btn.on('click', function (event) {
        event.preventDefault();
        var order_by = $(this).attr("data-sort");
        var data = {
            action: 'infinite_scroll',
            offset: get_offset(),
            sort_by: order_by
        };
        jQuery.post(ajaxurl, data, function (response) {
            $('.latest-blog-posts__wrapper').append(response);
            Waypoint.refreshAll();
        });
    });
    function hide_infinite(data) {
        jQuery.post(ajaxurl, data, function (response) {
            if (response.length < 1) {
                $infinitescroll_btn.hide();
            }
        });
    }
});