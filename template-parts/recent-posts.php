<?php
/**
 * Template part for displaying preview of posts
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */

?>
<div class="latest-blog-posts">
	<div class="container">
		<div class="latest-blog-posts__wrapper">
			<?php matchmaker_resent_posts( 3 ); ?>
		</div>
	</div>
</div>