<?php
/**
 * Template part for displaying preview of post without img
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 */
global $post;
$content = strip_shortcodes( $post->post_content );
$excerpt = wp_trim_words( $content, $num_words = 90, $more = null );
$time    = date( 'd M Y', strtotime( $post->post_date ) );
?>
<div class="latest-blog-item">
	<span><?php echo $time; ?></span>
	<a href="<?php echo get_permalink( $post->ID ); ?>">
		<h3><?php the_title(); ?></h3>
	</a>
	<p><?php echo $excerpt; ?></p>
</div>