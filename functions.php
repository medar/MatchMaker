<?php
/**
 * matchmaker functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 */

if ( ! function_exists( 'matchmaker_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function matchmaker_setup() {
		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );
		/*
		 * This theme uses wp_nav_menu() in one location.
		 */
		register_nav_menus( array(
			'menu-1' => esc_html( 'Primary' ),
			'menu-2' => esc_html( 'Footer menu' ),
		) );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );
//		set_post_thumbnail_size( 340, 300, true );
		add_image_size( 'post-preview', 340, 300, true );
		add_image_size( 'post-header', 1170, 500 );
		add_image_size( 'post-content', 670, 340 );
		add_image_size( 'matcher', 360, 426 );


	}
endif;
add_action( 'after_setup_theme', 'matchmaker_setup' );

function matchmaker_widgets_init() {
	register_sidebar( array(
		'name'          => 'Article sidebar',
		'id'            => 'sidebar-1',
		'description'   => 'Add widgets here.',
		'before_widget' => '<section id="%1$s" class="widget %2$s">',
		'after_widget'  => '</section>',
		'before_title'  => '<h4 class="widget-title">',
		'after_title'   => '</h4>',
	) );
}

add_action( 'widgets_init', 'matchmaker_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
if ( ! function_exists( 'matchmaker_scripts' ) ) :
	function matchmaker_scripts() {

		wp_enqueue_style(
			'matchmaker-libs',
			get_template_directory_uri() . '/css/libs.css',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'matchmaker-styles',
			get_template_directory_uri() . '/css/styles.css',
			array( 'matchmaker-libs' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'matchmaker-fonts',
			'https://fonts.googleapis.com/css?family=Open+Sans:300i,400,400i,600,700|Roboto+Condensed:700|Roboto:900',
			array(),
			'1.0.0',
			'all'
		);

		wp_enqueue_style(
			'matchmaker-style',
			get_stylesheet_uri(),
			array( 'matchmaker-styles', 'matchmaker-fonts' ),
			'1.0.0',
			'all'
		);

		wp_enqueue_script(
			'matchmaker-bootstrap',
			'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js',
			array( 'jquery' ),
			'3.3.6',
			true
		);

		wp_enqueue_script(
			'matchmaker-libs',
			get_template_directory_uri() . '/js/libs.js',
			array( 'matchmaker-bootstrap' ),
			'1.0.0',
			true
		);

		wp_enqueue_script(
			'matchmaker-js',
			get_template_directory_uri() . '/js/scripts.js',
			array( 'matchmaker-libs' ),
			'1.0.0',
			true
		);

	}
endif;

add_action( 'wp_enqueue_scripts', 'matchmaker_scripts' );


function matchmaker_posted_on() {
	$time_string = '<time class="entry-date published updated" datetime="%1$s">%2$s</time>';


	$time_string = sprintf( $time_string,
		esc_attr( get_the_date( 'c' ) ),
		esc_html( get_the_date() ),
		esc_attr( get_the_modified_date( 'c' ) ),
		esc_html( get_the_modified_date() )
	);

	$posted_on = sprintf(
		esc_html_x( '%s', 'post date', 'matchmaker' ),
		$time_string
	);

	echo '<span class="posted-on">' . $posted_on . '</span>';
}

function matchmaker_resent_posts( $posts_count = 3, $order_by = 'date', $posts_offset = 0 ) {
	$order = '';
	if ( $order_by == 'date' ) {
		$order = "DESC";
	} elseif ( $order_by == 'title' ) {
		$order = "ASC";
	}

	$args  = array(
		'posts_per_page'   => $posts_count,
		'post_type'        => 'post',
		'post_status'      => 'publish',
		'offset'           => $posts_offset,
		'orderby'          => $order_by,
		'order'            => $order,
		'suppress_filters' => true

	);
	$query = new WP_Query( $args );
	global $post;

	if ( $query->have_posts() ) {
		while ( $query->have_posts() ) {
			$query->the_post();
//			$post = $query->post;
//			setup_postdata( $post );

			if ( has_post_thumbnail( $post->ID ) ) :
				get_template_part( 'template-parts/post-preview' );
			else :
				get_template_part( 'template-parts/post-preview-no-img' );
			endif;
		}
		wp_reset_query();
		wp_reset_postdata();
	} else {
		wp_reset_query();
		wp_reset_postdata();
	}

}

function ajax_matchmaker_infinite_scroll_callback() {
	$offset  = intval( $_POST['offset'] );
	$sort_by = strval( $_POST['sort_by'] );
	matchmaker_resent_posts( $posts_count = 3, $order_by = $sort_by,  $posts_offset = $offset );

	wp_die();
}

add_action( 'wp_ajax_infinite_scroll', 'ajax_matchmaker_infinite_scroll_callback' );
add_action( 'wp_ajax_nopriv_infinite_scroll', 'ajax_matchmaker_infinite_scroll_callback' );



function matchmaker_subscribe() {
	$output = '';
	ob_start();
	?>
	<div class="subscribe">
		<div class="container">
			<div class="subscribe__wrapper">
				<h2>Sign Up for Newsletter</h2>
				<p><?php if ( function_exists( 'ot_get_option' ) ) {
						echo ot_get_option( 'sign_up_for_newsletter', '#' );
					} ?></p>
				<?php echo do_shortcode( '[contact-form-7 id="117" title="Sing up footer"]' ); ?>
			</div>
		</div>
	</div>
	<?php

	$output .= ob_get_clean();

	echo $output;
}


function matchmaker_tabs( $class = '' ) {
	$output = '';
	if ( function_exists( 'ot_get_option' ) ) {
		$links['mdg']['title']      = ot_get_option( 'mdg_title', '#' );
		$links['mdg']['link']       = ot_get_option( 'mdg_link', '#' );
		$links['dd']['title']       = ot_get_option( 'dd_title', '#' );
		$links['dd']['link']        = ot_get_option( 'dd_link', '#' );
		$links['bpct']['title']     = ot_get_option( 'business_performance_coaching_title', '#' );
		$links['bpct']['link']      = ot_get_option( 'business_performance_coaching_links', '#' );
		$links['swds']['title']     = ot_get_option( 'soho_wellness___day_spa_title', '#' );
		$links['swds']['link']      = ot_get_option( 'soho_wellness___day_spa_link', '#' );
		$links['wandc']['title']    = ot_get_option( 'workshops_and_classes_title', '#' );
		$links['wandc']['link']     = ot_get_option( 'workshops_and_classes_link', '#' );
		$links['pmatcher']['title'] = ot_get_option( 'professional_matcher_title', '#' );
		$links['pmatcher']['link']  = ot_get_option( 'professional_matcher_link', '#' );
	}
	?>
	<div class="index-tabs <?php echo $class; ?>">
		<ul>
			<li><a href="<?php echo $links['mdg']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG.png"
							 alt="<?php echo $links['mdg']['title']; ?>"
							 title="<?php echo $links['mdg']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['dd']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/DD.png"
							 alt="<?php echo $links['dd']['title']; ?>"
							 title="<?php echo $links['dd']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['bpct']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/BC_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/BC.png"
							 alt="<?php echo $links['bpct']['title']; ?>"
							 title="<?php echo $links['bpct']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['swds']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/SW.png"
							 alt="<?php echo $links['swds']['title']; ?>"
							 title="S<?php echo $links['swds']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['wandc']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/WS.png"
							 alt="<?php echo $links['wandc']['title']; ?>"
							 title="<?php echo $links['wandc']['title']; ?>">
					</picture>
				</a>
			</li>
			<li><a href="<?php echo $links['pmatcher']['link']; ?>">
					<picture>
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM@2x.png 2x"
								media="(min-width: 1200px)">
						<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_small@2x.png 2x"
								media="(max-width: 1199px)">
						<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/PM.png"
							 alt="<?php echo $links['pmatcher']['title']; ?>"
							 title="<?php echo $links['pmatcher']['title']; ?>">
					</picture>
				</a>
			</li>
		</ul>
	</div>

	<?php
	$output .= ob_get_clean();

	echo $output;
}

function register_slider_type() {
	register_post_type( 'matchmaker_reviews', array(
		'label'               => null,
		'labels'              => array(
			'name'               => 'Testimonials',
			'singular_name'      => 'Testimonial',
			'add_new'            => 'Add testimonial',
			'add_new_item'       => 'Add testimonial',
			'edit_item'          => 'Edit testimonial',
			'new_item'           => 'New testimonial',
			'view_item'          => 'View testimonial',
			'search_items'       => 'Search testimonial',
			'not_found'          => 'Not found',
			'not_found_in_trash' => 'Not found in trash',
			'parent_item_colon'  => '',
			'menu_name'          => 'Testimonials',
		),
		'description'         => '',
		'public'              => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'show_in_nav_menus'   => false,
		'show_in_rest'        => false,
		'rest_base'           => false,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-slides',
		//'capability_type'   => 'post',
		//'capabilities'      => 'post',
		//'map_meta_cap'      => null,
		'hierarchical'        => false,
		'supports'            => array( 'title', 'editor' ),
		// 'title','editor','author','thumbnail','excerpt','trackbacks','custom-fields','comments','revisions','page-attributes','post-formats'
		'taxonomies'          => array(),
		'has_archive'         => false,
		'rewrite'             => true,
		'query_var'           => true,
	) );
}

add_action( 'init', 'register_slider_type' );


remove_action( 'wp_head', 'dns-prefetch' );
add_filter( 'emoji_svg_url', '__return_false' );
remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
remove_action( 'wp_print_styles', 'print_emoji_styles' );
remove_action( 'wp_head', 'wp_resource_hints', 2 );