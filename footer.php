<footer class="site-footer">
	<div class="container">
		<div class="footer-top"><a class="footer-top__logo" href="/"><img
						src="<?php echo get_template_directory_uri(); ?>/img/logo--footer.svg" width="188"
						height="42" alt=""></a>
			<?php wp_nav_menu(
				array(
					'theme_location' => 'menu-2',
					'menu_id'        => 'footer-menu',
					'menu_class'     => 'footer-top__menu'
				)
			);
			?>
		</div>
		<div class="footer-bottom">
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$cop = ot_get_option( 'copyright', '#' );
			}
			?>
			<p><?php echo $cop; ?></p>
		</div>
	</div>
</footer>
<nav class="all-sites" id="allSites">
	<?php
	if ( function_exists( 'ot_get_option' ) ) {
		$links['mdg']['title'] = ot_get_option( 'mdg_title', '#' );
		$links['mdg']['link'] = ot_get_option( 'mdg_link', '#' );
		$links['dd']['title'] = ot_get_option( 'dd_title', '#' );
		$links['dd']['link'] = ot_get_option( 'dd_link', '#' );
		$links['bpct']['title'] = ot_get_option( 'business_performance_coaching_title', '#' );
		$links['bpct']['link'] = ot_get_option( 'business_performance_coaching_links', '#' );
		$links['swds']['title'] = ot_get_option( 'soho_wellness___day_spa_title', '#' );
		$links['swds']['link'] = ot_get_option( 'soho_wellness___day_spa_link', '#' );
		$links['wandc']['title'] = ot_get_option( 'workshops_and_classes_title', '#' );
		$links['wandc']['link'] = ot_get_option( 'workshops_and_classes_link', '#' );
		$links['pmatcher']['title'] = ot_get_option( 'professional_matcher_title', '#' );
		$links['pmatcher']['link'] = ot_get_option( 'professional_matcher_link', '#' );
	}
	?>
	<ul>
		<li><a href="<?php echo $links['mdg']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/MDG_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/MDG_small.png"
					     alt="<?php echo $links['mdg']['title']; ?>" title="<?php echo $links['mdg']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['dd']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/DD_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/DD_small.png"
					     alt="<?php echo $links['dd']['title']; ?>" title="<?php echo $links['dd']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['bpct']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>/img/tabs/BC_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri() . '/'; ?>/img/tabs/BC_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/BC_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/BC_small.png"
					     alt="<?php echo $links['bpct']['title']; ?>" title="<?php echo $links['bpct']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['swds']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/SW_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/SW_small.png"
					     alt="<?php echo $links['swds']['title']; ?>" title="<?php echo $links['swds']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['wandc']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/WS_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/WS_small.png"
					     alt="<?php echo $links['wandc']['title']; ?>" title="<?php echo $links['wandc']['title']; ?>">
				</picture>
			</a></li>
		<li><a href="<?php echo $links['pmatcher']['link']; ?>">
				<picture>
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_small@2x.png 2x"
					        media="(min-width: 768px)">
					<source srcset="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_xsmall.png 1x, <?php echo get_template_directory_uri(); ?>/img/tabs/PM_xsmall@2x.png 2x"
					        media="(max-width: 767px)">
					<img src="<?php echo get_template_directory_uri(); ?>/img/tabs/PM_small.png"
					     alt="<?php echo $links['pmatcher']['title']; ?>" title="<?php echo $links['pmatcher']['title']; ?>">
				</picture>
			</a></li>
	</ul>
</nav>
<?php wp_footer(); ?>
</body>
</html>