<?php get_header(); ?>
<main class="site-main site-main--index clearfix">
	<div class="index-first-screen">
		<div class="index-first-screen__img">
			<picture>
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/s1x1.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/s1x2.jpg 2x"
						media="(min-width: 992px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/s1x1--768.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/s1x2--768.jpg 2x"
						media="(min-width: 480px)">
				<source srcset="<?php echo get_template_directory_uri(); ?>/img/index/s1x1--480.jpg 1x, <?php echo get_template_directory_uri(); ?>/img/index/s1x2--480.jpg 2x"
						media="(max-width: 479px)">
				<img src="<?php echo get_template_directory_uri(); ?>/img/index/s1x2.jpg" alt="">
			</picture>
		</div>
		<div class="index-first-screen__video">
			<video id="video">
				<source src="<?php echo get_template_directory_uri(); ?>/video/540.mp4" type="video/mp4">
			</video>
		</div>
		<div class="index-first-screen__text">
			<svg xmlns="http://www.w3.org/2000/svg" width="196" height="173" viewbox="0 0 196 173">
				<path fill="#19BACC"
					  d="M147.333 140.508c21.434-18.47 42.76-45.102 47.784-80.21C199.93 26.67 173.83-2.42 139.384.34 123.4 1.622 108.58 10.624 97.77 21.045 86.96 10.625 72.34 1.62 56.36.34 21.91-2.423-4.187 26.67.625 60.3c10.54 73.672 92.67 110.02 97.144 111.947 1.122-.485 7.175-3.148 15.774-8.037-8.6 4.888-14.652 7.552-15.777 8.037.92-6.2 5.906-40.548 5.906-40.548l-.006-11.895-23.92-.12-.07-72.8h35.608v72.977s19.143 12.412 32.045 20.643z"></path>
				<path fill="#3198A3" d="M79.68 46.887l23.997 11.834v72.98L79.68 119.69"></path>
			</svg>
			<h1><?php echo get_bloginfo( 'name' ); ?></h1>
			<h2><?php echo get_bloginfo( 'description' ); ?></h2>
			<a class="btn btn--white" href="/get-started">Get Started </a>
		</div>
	</div>
	<?php matchmaker_tabs(); ?>
	<div class="index-cite cite">
		<div class="container">
			<div class="cite__wrapper">
				<h3><?php echo get_field( 'main_quote' ); ?></h3>
				<p><?php echo get_field( 'main_quote_author' ); ?></p>
			</div>
		</div>
	</div>
	<div class="index-main-text">
		<div class="container">
			<div class="index-main-text__wrapper">
				<?php
				while ( have_posts() ) : the_post();
					the_content();
				endwhile;
				?>
			</div>
		</div>
	</div>
	<div class="index-benefits">
		<div class="index-benefits__intro">
			<svg xmlns="http://www.w3.org/2000/svg" width="100" height="10.76" viewbox="0 0 100 10.76">
				<path d="M100,0.73Q86.45,7.56,69.31,7.56C52.16,7.56,45.42,0,28.88,0Q12.33,0,0,5.39v5.37H100v-10Z"></path>
			</svg>
		</div>
		<div class="index-benefits__content">
			<div class="container">
				<div class="index-benefits__content-wrapper">
					<div class="index-benefits__item">
						<div class="index-benefits__img"><img
									src="<?php echo get_template_directory_uri(); ?>/img/index/benefit1.svg"
									alt=""></div>
						<h3>Desire</h3>
						<p><?php echo get_field( 'desire' ); ?></p>
					</div>
					<div class="index-benefits__item">
						<div class="index-benefits__img"><img
									src="<?php echo get_template_directory_uri(); ?>/img/index/benefit2.svg"
									alt=""></div>
						<h3>Equality</h3>
						<p><?php echo get_field( 'equality' ); ?></p>
					</div>
					<div class="index-benefits__item">
						<div class="index-benefits__img"><img
									src="<?php echo get_template_directory_uri(); ?>/img/index/benefit3.svg"
									alt=""></div>
						<h3>Believe</h3>
						<p><?php echo get_field( 'believe' ); ?></p>
					</div>
				</div>
			</div>
		</div>
		<div class="index-benefits__outro">
			<svg xmlns="http://www.w3.org/2000/svg" width="100" height="10.76" viewbox="0 0 100 10.76">
				<path d="M100,0.73Q86.45,7.56,69.31,7.56C52.16,7.56,45.42,0,28.88,0Q12.33,0,0,5.39v5.37H100v-10Z"></path>
			</svg>

		</div>
	</div>
	<div class="index-reviews">
		<div class="container">


			<div class="index-reviews__wrapper">
				<div class="reviews-item__slider">
					<?php

					global $post;
					$args = array( 'posts_per_page' => 5, 'offset' => 0, 'post_type' => 'matchmaker_reviews' );

					$myposts = get_posts( $args );
					foreach ( $myposts as $post ) : setup_postdata( $post ); ?>
						<div class="index-reviews__item">
							<p><?php the_content(); ?></p>
							<cite><a href="#"><?php echo get_field( 'name' ); ?></a></cite>
						</div>
					<?php endforeach;
					wp_reset_postdata(); ?>

				</div>
				<div class="reviews-item__slider-controls"><span class="reviews-item__slider--left">
                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
                  <path class="pageFill"
						d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
                </svg></span><span class="reviews-item__slider--right">
                <svg xmlns="http://www.w3.org/2000/svg" width="7" height="13" data-name="PageArrow" viewbox="0 0 7 13">
                  <path class="pageFill"
						d="M.91 6.5l6-5.87a.36.36 0 0 0 0-.52.38.38 0 0 0-.53 0L.11 6.24a.36.36 0 0 0 0 .52l6.25 6.13a.38.38 0 0 0 .26.11.37.37 0 0 0 .26-.11.36.36 0 0 0 0-.52z"></path>
                </svg></span></div>
			</div>
		</div>
	</div>
	<div class="lottery index-lottery">
		<div class="container">
			<h2># Love Lottery</h2>
			<p><?php echo get_field( 'love_lottery_text' ); ?></p>
			<div class="lottery__wrapper">
				<div class="lottery__item">
					<h3>Be a Selector</h3>
					<p><?php echo get_field( 'selector' ); ?></p><a class="btn btn--turquoise"
																	href="/get-started">Get Started </a>
				</div>
				<div class="lottery__item">
					<h3>Be a Selectee </h3>
					<p><?php echo get_field( 'selectee' ); ?></p><a class="btn btn--turquoise"
																	href="/get-started">Get Started </a>
				</div>
			</div>
		</div>
	</div>
	<?php get_template_part( 'template-parts/recent-posts' ); ?>
	<?php matchmaker_subscribe(); ?>
</main>

<?php get_footer(); ?>
